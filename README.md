Instructions based on [https://spring.io/guides/gs/spring-boot-docker/](https://spring.io/guides/gs/spring-boot-docker/)

# REST-API Running in a Docker Container
 1. Build the SpringBoot application: `.gradlew build`
 2. (If you want to try running locally on the command line: `java -jar build/libs/gs-spring-boot-docker-0.1.0.jar`)
3. Build the Docker container: `docker build --build-arg JAR_FILE=build/libs/gs-spring-boot-docker-0.1.0.jar -t springio/gs-spring-boot-docker .`
4. Run the Docker container: `docker run -p 8080:8080 -t springio/gs-spring-boot-docker`
5. Access the API through `localhost:8080`

# REST API endpoints:

Method | Endpoint | Body | Return | Note
--- | --- | --- | --- | ---
POST | /customers/new | {"firstName": "*firstName*", "lastName": "*lastName*" } | *customerNumber* | Adds new customer to database
GET | /customers/*number* | | {"number" : *number*, "firstName": "*firstName*", "lastName": "*lastName*"} | Returns customer with given number
POST | /products/new | {"price": *price*, "description": *description*} | *sku* | Adds new product to database
GET | /products/*sku* | | {"sku": *sku*, "price": *price*, "description": *description*} | Returns product with given number
POST | /orders/new/*customerNumber* | | *orderNumber* | Adds new order to database for this customer
GET | /orders/*number*/customer | | *customerNumber* | Returns customer for this order number
GET | /orders/*number*/lines | | [{"sku": *sku*, "quantity": *quantity*}, ...] | Returns array of order lines for this order
PUT | /orders/*number*/lines | {"sku": *sku*, "quantity": *quantity*} | | Increase quantity of the product in this order
DELETE | /orders/*number*/lines | {"sku": *sku*, "quantity": *quantity*} | | Decrease quantity of the product in this order
POST | /shutdown | | | Shuts down the server
